<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\BackendController as Backend;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('about', [MainController::class, 'about'])->name('about');
Route::get('album', [MainController::class, 'album'])->name('album');
Route::get('news', [MainController::class, 'news'])->name('news');
Route::group(['prefix' => 'api-v1', 'as' => 'api.'], function(){
	// 關於我們
	Route::group(['as' => 'about.'], function(){
		Route::get('abouts', [Backend::class, 'aboutList'])->name('list');
		Route::get('about/{id}', [Backend::class, 'aboutDetail'])->name('detail');
		Route::post('about', [Backend::class, 'aboutCreate'])->name('create');
		Route::put('about/{id}', [Backend::class, 'aboutUpdate'])->name('update');
		Route::delete('about/{id}', [Backend::class, 'aboutDelete'])->name('delete');
	});
	// 相簿
	Route::group(['as' => 'album.'], function(){
		Route::get('albums', [Backend::class, 'albumList'])->name('list');
		Route::post('album', [Backend::class, 'albumCreate'])->name('create');
		Route::post('album/{id}', [Backend::class, 'albumUpdate'])->name('update');
		Route::get('album/{id}', [Backend::class, 'albumDetail'])->name('detail');
	});
});