<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mfiles', function (Blueprint $table) {
            $table->id();
            $table->integer('cid')->default(0)->comment('content_id');
            $table->string('file_path')->default('');
            $table->string('title')->default('');
            $table->unsignedInteger('sort')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mfiles');
    }
}
