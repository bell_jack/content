<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->unsignedSmallInteger('c_id')->default(0)->comment('type');
            $table->string('time', 10)->default('')->comment('日期');
            $table->text('content')->nullable()->comment('內容');
            $table->unsignedTinyInteger('is_show')->default(1)->comment('顯示');
            $table->unsignedTinyInteger('is_show_home')->default(0)->comment('顯示首頁');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
