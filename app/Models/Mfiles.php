<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mfiles extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'mfiles';

    const FILLABLE = [
        'cid',
        'file_path',
        'title',
        'sort',
    ];

    protected $fillable = self::FILLABLE;
    protected $casts = [
        
    ];
}
