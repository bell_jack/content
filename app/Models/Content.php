<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'content';

    const FILLABLE = [
        'subject',
        'c_id',
        'time',
        'content',
        'is_show',
        'is_show_home',

        'img',
    ];

    protected $fillable = self::FILLABLE;
    protected $casts = [
        'is_show' => 'boolean',
        'is_show_home' => 'boolean',
    ];
}
