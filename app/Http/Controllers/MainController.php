<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AboutService;
use App\Services\AlbumService;
use App\Services\NewsService;

class MainController extends Controller
{
	protected $aboutSrv;
	protected $albumSrv;
	protected $newsSrv;
    function __construct(
    	AboutService $aboutSrv,
    	AlbumService $albumSrv,
    	NewsService $newsSrv
    )
    {
    	$this->aboutSrv = $aboutSrv;
    	$this->albumSrv = $albumSrv;
    	$this->newsSrv = $newsSrv;
    }
    public function about()
    {
    	return $this->aboutSrv->getAdminList([]);
    }
    public function album()
    {
    	return $this->albumSrv->getAdminList([]);
    }
    public function news()
    {
    	return $this->newsSrv->getAdminList([]);
    }
}
