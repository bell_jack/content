<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AboutService;
use App\Services\AlbumService;
use App\Services\MfilesService;
use App\Services\CloudService;


class BackendController extends Controller
{
    protected $aboutSrv;
    protected $albumSrv;
    protected $mfilesSrv;
    protected $cloudSrv;

    function __construct(
    	AboutService $aboutSrv,
        AlbumService $albumSrv,
        MfilesService $mfilesSrv,
        CloudService $cloudSrv
    )
    {
    	$this->aboutSrv = $aboutSrv;
        $this->albumSrv = $albumSrv;
    	$this->mfilesSrv = $mfilesSrv;
    	$this->cloudSrv = $cloudSrv;
    }
    public function aboutList(Request $req)
    {
    	$get = $req->all();
    	$data = $this->aboutSrv->getAdminList($get, []);
        return response()->json(['success' => true, 'data' => $data]);
    }
    public function aboutDetail($id)
    {
    	$data = $this->aboutSrv->getDetail($id);
        return response()->json(['success' => true, 'data' => $data]);
    }
    public function aboutCreate(Request $req)
    {
    	$post = $req->all();
    	$rs = $this->aboutSrv->AdminCreate($post);
        return response()->json(['success' => $rs]);
    }
    public function aboutUpdate(Request $req, $id)
    {
        $post = $req->all();
        $rs = $this->aboutSrv->AdminUpdate($id, $post);
        return response()->json(['success' => $rs]);
    }
    public function aboutDelete($id)
    {
        $rs = $this->aboutSrv->AdminDelete($id);
        return response()->json(['success' => $rs]);
    }
    public function albumList(Request $req)
    {
        $get = $req->all();
        $data = $this->albumSrv->getAdminList($get, []);
        return response()->json(['success' => true, 'data' => $data]);
    }
    public function albumCreate(Request $req)
    {
        $post = $req->all();
        
        $img_field = 'img';
        if ($req->hasFile($img_field)) {
            $file = $req->file($img_field);
            $error = $this->cloudSrv->fileCheck($file, $post, $img_field, 'img', 1000);
            if (!empty($error)) return response()->json(['success' => false, 'msg' => $error]);
        }
        $photo_field = 'photo';
        if ($req->hasFile($photo_field)) {
            $files = $req->file($photo_field);
            $error = $this->cloudSrv->filesCheck($files, $post, $photo_field, 'img', 1000);
            if (!empty($error)) return response()->json(['success' => false, 'msg' => $error]);
        }


        if ($req->hasFile($img_field)) {
            $file = $req->file($img_field);
            $error = $this->cloudSrv->fileCheck($file, $post, $img_field, 'img', 1000);
            if (!empty($error)) return response()->json(['success' => false, 'msg' => $error]);
            $file = $this->cloudSrv->fileUpload($file);
            $post['img'] = $file['file_name'];
        }
        $content = $this->albumSrv->AdminCreate($post);
        if ($req->hasFile($photo_field)) {
            $files = $req->file($photo_field);
            $mfile = [];
            $rs_sort = $this->mfilesSrv->MaxSort($id)->toArray();
            $sort = $rs_sort['sort'] + 1;
            foreach ($files as $file) {
                $tmp = $this->cloudSrv->fileUpload($file);
                $tmp['cid'] = $content['id'];
                $tmp['file_path'] = $tmp['file_name'];
                $tmp['title'] = $tmp['orig_name'];
                $tmp['sort'] = $sort++;
                $this->mfilesSrv->AdminCreate($tmp);
            }
        }
        return response()->json(['success' => true, 'msg' => '上傳成功']);
    }
    public function albumDetail($id)
    {
        $data = $this->albumSrv->getDetail($id);
        $data['mfile'] = $this->mfilesSrv->getList($id);
        return response()->json(['success' => true, 'data' => $data]);
    }
    public function albumUpdate(Request $req, $id)
    {
        $post = $req->all();
        // img upload check
        $img_field = 'img';
        if ($req->hasFile($img_field)) {
            $file = $req->file($img_field);
            $error = $this->cloudSrv->fileCheck($file, $post, $img_field, 'img', 1000);
            if (!empty($error)) return response()->json(['success' => false, 'msg' => $error]);
        }
        $photo_field = 'photo';        
        if ($req->hasFile($photo_field)) {
            $files = $req->file($photo_field);
            $error = $this->cloudSrv->filesCheck($files, $post, $photo_field, 'img', 1000);
            if (!empty($error)) return response()->json(['success' => false, 'msg' => $error]);
        }
        if (!empty($post['file_delete'])) {
            $ids = array_filter(explode(',', $post['file_delete']));
            foreach ($ids as $key => $fid) {
                $this->mfilesSrv->AdminDelete($fid);
            }
        }
        if (!empty($post['file_id'])) {
            foreach ($post['file_id'] as $key => $fid) {
                $data = [
                    'title' => $post['file_title'][$key],
                    'sort' => $post['file_sort'][$key],
                ];
                $this->mfilesSrv->AdminUpdate($fid, $data);
            }
        }
        
        if ($req->hasFile($img_field)) {
            $file = $req->file($img_field);
            $file = $this->cloudSrv->fileUpload($file);
            $post['img'] = $file['file_name'];
        }
        $content = $this->albumSrv->AdminUpdate($id, $post);

        if ($req->hasFile($photo_field)) {
            $files = $req->file($photo_field);
            $error = $this->cloudSrv->filesCheck($files, $post, $photo_field, 'img', 1000);
            if (!empty($error)) return response()->json(['success' => false, 'msg' => $error]);
            $mfile = [];
            $rs_sort = $this->mfilesSrv->MaxSort($id)->toArray();
            $sort = $rs_sort['sort'] + 1;
            foreach ($files as $file) {
                $tmp = $this->cloudSrv->fileUpload($file);
                $tmp['cid'] = $content['id'];
                $tmp['file_path'] = $tmp['file_name'];
                $tmp['title'] = $tmp['orig_name'];
                $tmp['sort'] = $sort++;
                $this->mfilesSrv->AdminCreate($tmp);
            }
        }
        return response()->json(['success' => true, 'msg' => '更新成功']);
    }
}
