<?php 
namespace App\Repositories;

use App\Models\Mfiles;

class MfilesRepository
{
	protected $mfiles;

	function __construct(Mfiles $mfiles)
	{
		$this->mfiles = $mfiles;
	}
	public function getMaxSort($cid)
	{
		return $this->mfiles->where('cid', $cid)
			->select(\DB::raw('MAX(sort) as sort'))
			->first();
	}
	public function getList($cid)
	{
		return $this->mfiles->where('cid', $cid)
			->orderBy('sort', 'asc')
			->get()->toArray();
	}
	public function create($data)
	{
		return $this->mfiles->create($data);
	}
	public function update($id, $data)
	{
		return $this->mfiles->where('id', $id)
			->update($data);
	}
	public function delete($id)
	{
		return $this->mfiles->where('id', $id)->delete();
	}
	public function deleteList($cid)
	{
		return $this->mfiles->where('cid', $cid)->delete();
	}
}