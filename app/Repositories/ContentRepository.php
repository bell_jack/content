<?php 
namespace App\Repositories;

use App\Models\Content;

class ContentRepository
{
	protected $c_type = null;
	const CID = [
		'about' => 2,
		'news' => 3,
		'service' => 4,
		'product' => 5,
		'album' => 6,
		'download' => 7
	];
	protected $content;

	function __construct(Content $content)
	{
		$this->content = $content;
	}
	public function getCid()
	{
		return self::CID[$this->c_type];
	}
	public function setCid($type)
	{
		$this->c_type = $type;
	}
	public function getCols()
	{
		return Content::FILLABLE;
	}
	public function getDetail($id)
	{
		return $this->content->where('id', $id)->where('c_id', $this->getCid())->first();
	}
	public function create($data)
	{
		if (!isset($data['c_id'])) $data['c_id'] = $this->getCid();
		return $this->content->create($data);
	}
	public function update($id, $data)
	{
		return $this->content->where('id', $id)
			->update($data);
	}
	public function delete($id)
	{
		return $this->content->where('id', $id)->delete();
	}
	public function getAdminList($get, $search)
	{
		$query = $this->content->where('c_id', $this->getCid());
		if (!empty($get['where'])) {
			foreach ($get['where'] as $key => $value) {
				if ($key === 'c_id') continue;
				$query->where($key, $value);
			}
		}
		if (!empty($search['key']) && !empty($search['range'])) {
			$q = $search['key'];
			$range = $search['range'];
			$query->where(function ($query) use ($q, $range) {
				foreach ($range as $cols) {
					$query->orWhere($cols, 'like', '%'.$q.'%');
				}
			});
		}
		return $query->get()->toArray();
	}
}