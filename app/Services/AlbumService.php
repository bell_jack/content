<?php 
namespace App\Services;

use App\Repositories\ContentRepository;

class AlbumService
{
	protected $contentRepo;

	function __construct(ContentRepository $contentRepo)
	{
		$this->contentRepo = $contentRepo;
		$this->contentRepo->setCid('album');
	}
	public function getAdminList($get)
	{
		return $this->contentRepo->getAdminList($get, []);
	}
	public function getDetail($id)
	{
		return $this->contentRepo->getDetail($id);
	}
	public function AdminCreate($data)
	{
		$cols = $this->contentRepo->getCols();
		foreach ($data as $key => $value) {
			if (!in_array($key, $cols)) unset($data[$key]);
		}
		return $this->contentRepo->create($data);
	}
	public function AdminUpdate($id, $data)
	{
		$cols = $this->contentRepo->getCols();
		foreach ($data as $key => $value) {
			if (!in_array($key, $cols)) unset($data[$key]);
		}
		return $this->contentRepo->update($id, $data);
	}
	public function AdminDelete($id)
	{
		return $this->contentRepo->delete($id);
	}
}