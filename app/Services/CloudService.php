<?php 
namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CloudService
{
	private $types = [
			'file' => 'doc,docx,pdf,csv,xls,xlsx,ppt,pptx',
			'img' => 'gif,jpg,bmp,jpeg,png'
		];
	private $ext = [
			'file' => ['doc','docx','pdf','csv','xls','xlsx','ppt','pptx'],
			'img' => ['gif','jpg','bmp','jpeg','png']
		];
	public function fileCheck($file, $post, $field, $type, $size)
	{
		$ext = $file->getClientOriginalExtension();
		if (!in_array($ext, $this->ext[$type])) return '格式錯誤';
		$ar[$field] = 'mimes:'.$this->types[$type].'|max:'.$size;
		$validator = Validator::make($post, $ar);
		if ($validator->fails()) {
			$file_name = $file->getClientOriginalName();
			return '檔案格式或大小不符';
			// return $validator->errors()->first($field);
		}
		return '';
	}
	public function filesCheck($files, $post, $field, $type, $size)
	{
		$validate = 'mimes:'.$this->types[$type].'|max:'.$size;
		$error = [];
		foreach ($files as $key => $file) {
			$ext = $file->getClientOriginalExtension();
			$file_name = $file->getClientOriginalName();
			if (!in_array($ext, $this->ext[$type])) return '格式錯誤';

		    $index = $field.'.'.$key;
		    $validator = Validator::make($post, [$index => $validate]);
		    if ($validator->fails()) {
		        return '檔案格式或大小不符';
		    }
		}
		return implode(',', $error);
	}
	public function fileUpload($file)
	{
		$f['orig_name'] = $file->getClientOriginalName();
		$f['file_name'] = $file->hashName();
		Storage::disk('public')->put('upload', $file);
		return $f;
	}
}