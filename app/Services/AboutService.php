<?php 
namespace App\Services;

use App\Repositories\ContentRepository;

class AboutService
{
	protected $contentRepo;

	function __construct(ContentRepository $contentRepo)
	{
		$this->contentRepo = $contentRepo;
		$this->contentRepo->setCid('about');
	}
	public function getAdminList($get)
	{
		return $this->contentRepo->getAdminList($get, []);
	}
	public function getDetail($id)
	{
		return $this->contentRepo->getDetail($id);
	}
	public function AdminCreate($data)
	{
		return $this->contentRepo->create($data);
	}
	public function AdminUpdate($id, $data)
	{
		return $this->contentRepo->update($id, $data);
	}
	public function AdminDelete($id)
	{
		return $this->contentRepo->delete($id);
	}
}