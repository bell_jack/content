<?php 
namespace App\Services;

use App\Repositories\MfilesRepository;

class MfilesService
{
	protected $mfilesRepo;

	function __construct(MfilesRepository $mfilesRepo)
	{
		$this->mfilesRepo = $mfilesRepo;
	}
	public function getList($id)
	{
		return $this->mfilesRepo->getList($id);
	}
	// public function getDetail($id)
	// {
	// 	return $this->mfilesRepo->getDetail($id);
	// }
	public function AdminCreate($data)
	{
		return $this->mfilesRepo->create($data);
	}
	public function MaxSort($cid)
	{
		return $this->mfilesRepo->getMaxSort($cid);
	}
	public function AdminUpdate($f_id, $data)
	{
		return $this->mfilesRepo->update($f_id, $data);
	}
	public function AdminDelete($f_id)
	{
		return $this->mfilesRepo->delete($f_id);
	}
	public function listDelete($id)
	{
		return $this->mfilesRepo->deleteList($id);
	}
}