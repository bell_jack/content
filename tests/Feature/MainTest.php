<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MainTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_about()
    {
        $response = $this->get(route('about'));
        $response->assertStatus(200);
    }
    public function test_album()
    {
        $response = $this->get(route('album'));
        $response->assertStatus(200);
    }
    public function test_news()
    {
        $response = $this->get(route('news'));
        $response->assertStatus(200);
    }
}
