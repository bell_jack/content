<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use App\Models\Content;

class BackendTest extends TestCase
{
    // use RefreshDatabase;
    /**
     * A basic feature tet example.
     *
     * @return void
     */
    public function tet_aboutList()
    {
        $response = $this->get(route('api.about.list'));
        $this->assertEquals(200, $response->getStatusCode());   
    }
    public function tet_aboutCreate()
    {
        $response = $this->post(route('api.about.create'), array(
            'subject' => 'abc',
        ));
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function tet_aboutDetail()
    {
        $id = 2;
        $response = $this->get(route('api.about.detail', $id));
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function tet_aboutUpdate()
    {
        $id = 2;
        $response = $this->put(route('api.about.update', $id), [
            'subject' => 'bb',
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function tet_aboutDelete()
    {
        $id = 3;
        $response = $this->delete(route('api.about.delete', $id));
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function tet_albumCreate_imgSizeError()
    {
        $img = UploadedFile::fake()->create('image.jpg', 10000);
        $response = $this->post(route('api.album.create'), [
            'img' => $img
        ]);
        $json = $response->decodeResponseJson()->json;
        // print_r(json_decode($json));
        $response->assertJson(['success' => false, 'msg' => '檔案格式或大小不符'], $json);
    }
    public function tet_albumCreate_imgExtError()
    {
        $img = UploadedFile::fake()->create('src.pdf', 100);
        $response = $this->post(route('api.album.create'), [
            'img' => $img
        ]);
        $json = $response->decodeResponseJson()->json;
        // print_r(json_decode($json));
        $response->assertJson(['success' => false, 'msg' => '格式錯誤'], $json);
    }
    public function tet_albumCreate_imgMimeError()
    {
        // $img = UploadedFile::fake()->image('avatar.jpg');
        $img = UploadedFile::fake()->create(
            'document.jpg', 1000, 'application/pdf'
        );
        // $photos = [];
        // for ($i = 1; $i <=3; $i++) {
        //     $name = 'img'.$i.'.pdf';
        //     $photos[] = UploadedFile::fake()->create($name, 100);
        //     // $photos[] = UploadedFile::fake()->image($name);
        // }
        $response = $this->post(route('api.album.create'), [
            'subject' => 'album1',
            'img' => $img,
            // 'photo' => $photos
        ]);
        $json = $response->decodeResponseJson()->json;
        $response->assertJson(['success' => false, 'msg' => '檔案格式或大小不符'], $json);
    }
    public function test_albumCreate_succ()
    {
        Storage::fake('public');
        // Call to undefined function Illuminate\Http\Testing\imagecreatetruecolor()
        $img = UploadedFile::fake()->image('avatar.jpg');
        $photos = [];
        for ($i = 1; $i <=3; $i++) {
            $name = 'img'.$i.'.jpg';
            $photos[] = UploadedFile::fake()->create($name, 100);
        }
        $response = $this->post(route('api.album.create'), [
            'subject' => 'album1',
            'img' => $img,
            'photo' => $photos
        ]);
        // $json = $response->decodeResponseJson()->json;
        $db = Content::latest()->first();
        // print_r($db['img']);

        $this->assertEquals($img->hashName(), $db->img);

        // $response->assertJson(['success' => true, 'msg' => '上傳成功'], $json);
    }
    public function test_albumDetail()
    {
        $id = 1;
        $response = $this->get(route('api.album.detail', $id));
        $json = $response->decodeResponseJson()->json;
        // print_r($json);
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function test_albumUpdate()
    {
        $img = UploadedFile::fake()->image('avatar.jpg');
        $photos = [];
        for ($i = 1; $i <=3; $i++) {
            $name = 'img'.$i.'.jpg';
            $photos[] = UploadedFile::fake()->create($name, 100);
        }
        $subject = 'album3';
        $id = 1;
        $response = $this->post(route('api.album.update', $id), [
            'subject' => $subject,
            // 'img' => $img,
            // 'photo' => $photos
            'file_id' => [],
            'file_title' => [],
            'file_sort' => [],
            'file_delete' => ''
        ]);
        // $db = Content::latest()->first();
        // $this->assertEquals($img->hashName(), $db->img);
        // $this->assertEquals($subject, $db->subject);
        $json = $response->decodeResponseJson()->json;
        $response->assertJson(['success' => true, 'msg' => '更新成功'], $json);
    }
}
