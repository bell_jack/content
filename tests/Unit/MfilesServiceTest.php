<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\MfilesService;
use App\Repositories\MfilesRepository;
use Mockery;

class MfilesServiceTest extends TestCase
{
	protected $Repo;

    protected function setUp(): void
    {
    	parent::setUp();
        // $this->Repo = MfilesRepository::class;
        $this->initMock(MfilesRepository::class);
    }
    protected function initMock($class)
    {
    	$this->Repo = Mockery::mock($class);
        // $this->Repo->shouldReceive('setCid')->andReturn(6);
    }
    public function test_MaxSort()
    {
        $this->Repo->shouldReceive('getMaxSort')->andReturn([1,2]);
        $mfilesService = new MfilesService($this->Repo);
        $get = 1;
        $this->assertEquals([1, 2], $mfilesService->MaxSort($get));
    }
}
