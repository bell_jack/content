<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\AlbumService;
use App\Repositories\ContentRepository;
use Mockery;

class AlbumServiceTest extends TestCase
{
	protected $contentRepo;

    protected function setUp(): void
    {
    	parent::setUp();
        $this->initMock(ContentRepository::class);
    }
    protected function initMock($class)
    {
    	$this->contentRepo = Mockery::mock($class);
        $this->contentRepo->shouldReceive('setCid')->andReturn(6);
    }
    public function test_getAdminList()
    {
        $this->contentRepo->shouldReceive('getAdminList')->andReturn([1,2]);
        $albumService = new AlbumService($this->contentRepo);
        $get = ['where' => []];
        $this->assertEquals([1, 2], $albumService->getAdminList($get));
    }
    public function test_adminCreate()
    {
        $this->contentRepo->shouldReceive('create')->andReturn(true);
        $albumService = new AlbumService($this->contentRepo);
        $data['subject'] = '';
        $this->assertEquals(true, $albumService->AdminCreate($data));
    }
    public function test_adminUpdate()
    {
        $this->contentRepo->shouldReceive('update')->andReturn(true);
        $albumService = new AlbumService($this->contentRepo);
        $data['subject'] = '';
        $this->assertEquals(true, $albumService->AdminUpdate(1, $data));
    }
    public function test_AdminDelete()
    {
        $this->contentRepo->shouldReceive('delete')->andReturn(true);
        $albumService = new AlbumService($this->contentRepo);
        $id = 1;
        $this->assertEquals(true, $albumService->AdminDelete($id));
    }
}
