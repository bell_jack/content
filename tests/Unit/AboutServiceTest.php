<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\AboutService;
use App\Repositories\ContentRepository;
use Mockery;

class AboutServiceTest extends TestCase
{
	protected $contentRepo;

    protected function setUp(): void
    {
    	parent::setUp();
        $this->initMock(ContentRepository::class);
    }
    protected function initMock($class)
    {
        $this->contentRepo = Mockery::mock($class);
        $this->contentRepo->shouldReceive('setCid')->andReturn(2);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_getAdminList()
    {
        $this->contentRepo->shouldReceive('getAdminList')->andReturn([1,2]);
        $aboutService = new AboutService($this->contentRepo);
        $get = ['where' => []];
        $this->assertEquals([1, 2], $aboutService->getAdminList($get));
    }
    public function test_getDetail()
    {
        $return = ['subject' => 'test'];
        $this->contentRepo->shouldReceive('getDetail')->andReturn($return);
        $aboutService = new AboutService($this->contentRepo);
        $id = 1;
        $this->assertEquals($return, $aboutService->getDetail($id));
    }
    public function test_adminCreate()
    {
        $this->contentRepo->shouldReceive('create')->andReturn(true);
        $aboutService = new AboutService($this->contentRepo);
        $data['subject'] = '';
        $this->assertEquals(true, $aboutService->AdminCreate($data));
    }
    public function test_adminUpdate()
    {
        $this->contentRepo->shouldReceive('update')->andReturn(true);
        $aboutService = new AboutService($this->contentRepo);
        $data['subject'] = '';
        $this->assertEquals(true, $aboutService->AdminUpdate(1, $data));
    }
    public function test_AdminDelete()
    {
        $this->contentRepo->shouldReceive('delete')->andReturn(true);
        $aboutService = new AboutService($this->contentRepo);
        $id = 1;
        $this->assertEquals(true, $aboutService->AdminDelete($id));
    }
}
