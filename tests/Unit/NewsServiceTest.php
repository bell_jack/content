<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\NewsService;
use App\Repositories\ContentRepository;
use Mockery;

class NewsServiceTest extends TestCase
{
	protected $contentRepo;

    protected function setUp(): void
    {
    	parent::setUp();
        $this->initMock(ContentRepository::class);
    }
    protected function initMock($class)
    {
    	$this->contentRepo = Mockery::mock($class);
        $this->contentRepo->shouldReceive('setCid')->andReturn(3);
    }
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_getAdminList()
    {
        $this->contentRepo->shouldReceive('getAdminList')->andReturn([1,2]);
        $newsService = new NewsService($this->contentRepo);
        $get = ['where' => []];
        $this->assertEquals([1, 2], $newsService->getAdminList($get));
    }
    public function test_adminCreate()
    {
        $this->contentRepo->shouldReceive('create')->andReturn(true);
        $newsService = new NewsService($this->contentRepo);
        $data['subject'] = '';
        $this->assertEquals(true, $newsService->AdminCreate($data));
    }
    public function test_adminUpdate()
    {
        $this->contentRepo->shouldReceive('update')->andReturn(true);
        $newsService = new NewsService($this->contentRepo);
        $data['subject'] = '';
        $this->assertEquals(true, $newsService->AdminUpdate(1, $data));
    }
    public function test_AdminDelete()
    {
        $this->contentRepo->shouldReceive('delete')->andReturn(true);
        $newsService = new NewsService($this->contentRepo);
        $id = 1;
        $this->assertEquals(true, $newsService->AdminDelete($id));
    }
}
